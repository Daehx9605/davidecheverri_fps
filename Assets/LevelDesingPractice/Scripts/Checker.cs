﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checker : MonoBehaviour
{
    private Move move;
    private Rotation rotation;

    private void Awake()
    {
        move = GetComponent<Move>();
        rotation = GetComponent<Rotation>();
    }


    public void Check()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, -transform.up, out hit, 3))
        {
            if (transform.position.z >= hit.collider.bounds.max.z)
            {
                Debug.Log("en el limite");
            }

        }
        else
        {
            if (move.getCanmove)
            {
                move.CantMove(false);
                rotation.PlayerRotation();
            }
        }
    }

    private void FixedUpdate()
    {
        
    }


}