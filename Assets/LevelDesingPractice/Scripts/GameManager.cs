﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject Enemy;
    private GameObject[] posEnemys;

    void Start()
    {
        posEnemys = GameObject.FindGameObjectsWithTag("EnemyPos");
    }

    void Update()
    {
        if (GameObject.FindGameObjectsWithTag("Enemy").Length == 0)
        {
            Instantiate(Enemy, posEnemys[Random.Range(0, posEnemys.Length)].transform);
        }
    }

    
}
