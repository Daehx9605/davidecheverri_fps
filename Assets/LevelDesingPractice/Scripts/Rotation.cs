﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    private Move move;

    private void Awake()
    {
        move = GetComponent<Move>();
    }
    public void PlayerRotation()
    {
        transform.Rotate(90, 0, 0);
        transform.forward += new Vector3(0, 0, 0.5f);
        move.CantMove(true);
        //transform.position = transform.position + new Vector3 (0, 1, 0);
    }
}
